﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.CommonMenu
{
    public static class Menu
    {
        public static class UserTab
        {
            public static void OpenUserTabMenu()
            {
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav user_menu pull-right']/li[4]/a")).Click();
                WebDriver.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            }

            public static void ClickSignOut()
            {
                OpenUserTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav user_menu pull-right']/li[4]//ul/li[5]/a")).Click();
            }
        }

        public static class AccountsTab
        {
            public static void OpenAccountsTabMenu()
            {
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[1]/a")).Click();
                WebDriver.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            }

            public static void ClickCompany()
            {
                OpenAccountsTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[1]//ul/li[1]/a")).Click();
            }

            public static void ClickRoute()
            {
                OpenAccountsTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[1]//ul/li[2]/a")).Click();
            }

            public static void ClickTerminal()
            {
                OpenAccountsTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[1]//ul/li[3]/a")).Click();
            }

            public static void ClickUser()
            {
                OpenAccountsTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[1]//ul/li[4]/a")).Click();
            }
        }

        public static class SystemTab
        {
            public static void OpenSystemTabMenu()
            {
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[4]/a")).Click();
                WebDriver.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            }

            public static void ClickAudit()
            {
                OpenSystemTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[4]//ul/li[1]/a")).Click();
            }

            public static void ClickAdministrator()
            {
                OpenSystemTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[4]//ul/li[2]/a")).Click();
            }

            public static void ClickRole()
            {
                OpenSystemTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[4]//ul/li[3]/a")).Click();
            }

            public static void ClickConfig()
            {
                OpenSystemTabMenu();
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav']/li[4]//ul/li[4]/a")).Click();
            }
        }
    }
}
