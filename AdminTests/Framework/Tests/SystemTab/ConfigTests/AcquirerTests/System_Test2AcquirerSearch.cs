﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.AcquirerTests
{
    class SystemTest2AcquirerSearch: Base
    {
        [Test]
        public void AcquirerSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickAcquirerMenuItem();
            AcquirerPage.SearchAcquirer();
            Assert.IsTrue(AcquirerPage.IsAcquirerWasFound());

        }
    }
    
}
