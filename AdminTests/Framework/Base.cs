﻿using System;
using System.Configuration;
using AdminTests.Domain.CommonMenu;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace AdminTests.Framework
{
    [TestFixture]
    class Base
    {
        public static string SmokeTestLogin;
        public static string SmokeTestPassword;

        public static string SitePath;
        public static string RemoteDriverUrl;
        public static string TestBrowser;
        public static string TestBrowserVersion;
        public static string TestPlatform;
        public static string SauceUserName;
        public static string SauceAccessKey;

        public Base()
        {
            SmokeTestLogin = ConfigurationManager.AppSettings["SmokeTestLogin"];
            SmokeTestPassword = ConfigurationManager.AppSettings["SmokeTestPassword"];
            SitePath = ConfigurationManager.AppSettings["SitePath"];
            RemoteDriverUrl = ConfigurationManager.AppSettings["RemoteDriverUrl"];
            TestBrowser = ConfigurationManager.AppSettings["TestBrowser"];
            TestBrowserVersion = ConfigurationManager.AppSettings["TestBrowserVersion"];
            TestPlatform = ConfigurationManager.AppSettings["TestPlatform"];
            SauceUserName = ConfigurationManager.AppSettings["SauceUserName"];
            SauceAccessKey = ConfigurationManager.AppSettings["SauceAccessKey"];
        }

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //WebDriver.Driver = new FirefoxDriver(); //uncomment for runnning tests on local mashine with browser Firefox 
            //WebDriver.Driver = new ChromeDriver("../../"); //uncomment for runnning tests on local mashine with browser Chrome
            //WebDriver.Driver = new InternetExplorerDriver("../../"); //uncomment for runnning tests on local mashine with browser Internet Explorer


            //uncomment the part below for runnning tests with sauce lab.
            var capabillities = new DesiredCapabilities();
            switch (TestBrowser)
            {
                case "Android":
                    capabillities = DesiredCapabilities.Android();
                    break;
                case "Chrome":
                    capabillities = DesiredCapabilities.Chrome();
                    break;
                case "Firefox":
                    capabillities = DesiredCapabilities.Firefox();
                    break;
                case "HtmlUnit":
                    capabillities = DesiredCapabilities.HtmlUnit();
                    break;
                case "HtmlUnitWithJavaScript":
                    capabillities = DesiredCapabilities.HtmlUnitWithJavaScript();
                    break;
                case "IPad":
                    capabillities = DesiredCapabilities.IPad();
                    break;
                case "IPhone":
                    capabillities = DesiredCapabilities.IPhone();
                    break;
                case "InternetExplorer":
                    capabillities = DesiredCapabilities.InternetExplorer();
                    break;
                case "Opera":
                    capabillities = DesiredCapabilities.Opera();
                    break;
                case "PhantomJS":
                    capabillities = DesiredCapabilities.PhantomJS();
                    break;
                case "Safari":
                    capabillities = DesiredCapabilities.Safari();
                    break;
            }

            capabillities.SetCapability(CapabilityType.Version, TestBrowserVersion);

            capabillities.SetCapability(CapabilityType.Platform, TestPlatform);
            capabillities.SetCapability("name", "Automated Tests for Admin");
            capabillities.SetCapability("username", SauceUserName);
            capabillities.SetCapability("accessKey", SauceAccessKey);
            capabillities.SetCapability("idle-timeout", 200000);
            WebDriver.Driver = new RemoteWebDriver(new Uri(RemoteDriverUrl), capabillities);
        }

        [TearDown]
        public void TearDown()
        {
            Menu.UserTab.ClickSignOut();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            WebDriver.Close();
        }

        [SetUp]
        public void SetUp()
        {
            WebDriver.Driver.Manage().Window.Maximize();
            WebDriver.GoToUrl(SitePath);
        }
    }
}
