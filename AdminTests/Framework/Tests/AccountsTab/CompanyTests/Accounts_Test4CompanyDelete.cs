﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.AccountsTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.AccountsTab.CompanyTests
{
    class AccountsTest4CompanyDelete : Base
    {
        [Test]
        public void CompanyDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.AccountsTab.ClickCompany();
            CompanyPage.DeleteCompany();
            Assert.IsTrue(CompanyPage.IsCompanyWasDeleted());
        }
    }
}