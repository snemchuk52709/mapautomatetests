﻿using System;
using System.Configuration;
using AdminTests.Domain.Pages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests
{
    class LoginTest : Base
    {
        public static string TestLoginLogin;
        public static string TestLoginPassword;

        static LoginTest()
        {
            TestLoginLogin = ConfigurationManager.AppSettings["TestLoginLogin"];
            Assert.IsFalse(String.IsNullOrEmpty(TestLoginLogin), "No App.Config found.");
            TestLoginPassword = ConfigurationManager.AppSettings["TestLoginPassword"];
            Assert.IsFalse(String.IsNullOrEmpty(TestLoginPassword), "No App.Config found.");
        }
        [Test]
        public void SuccessLoginTest()
        {
            LoginPage.DoLogin(TestLoginLogin, TestLoginPassword);
            Assert.IsTrue(WebDriver.GetPageTitle() == "Home");
        }
    }
}