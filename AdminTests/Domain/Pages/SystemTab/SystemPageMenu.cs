﻿using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab
{
    class SystemPageMenu
    {
        private const string CommonXPath = "//div[@id = 'side_accordion']/div[";

        public static void ClickAudit()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "1]")).Click();
        }

        public static void ClickAdministrator()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "2]")).Click();
        }

        public static void ClickRole()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "3]")).Click();
        }

        public static void ClickConfig()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "4]")).Click();
        }
    }
}
