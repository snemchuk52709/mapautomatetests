﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.ModuleTests
{
    class SystemTest1ModuleAdd : Base
    {
        [Test]
        public void ModuleAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickModuleMenuItem();
            ModulePage.CreateModule();
            Assert.IsTrue(ModulePage.AddModule_IsModulewasCreated());
        }
    }
}
