﻿using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.AccountsTab
{
    class AccountsPageMenu
    {
        private const string CommonXPath = "//div[@id = 'side_accordion']/div[";

        public static void ClickCompany()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "1]")).Click();
        }

        public static void ClickRoute()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "2]")).Click();
        }

        public static void ClickTerminal()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "3]")).Click();
        }

        public static void ClickUser()
        {
            WebDriver.Driver.FindElement(By.XPath(CommonXPath + "4]")).Click();
        }
    }
}