﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.AcquirerTests
{
    internal class SystemTest1AcquirerAdd : Base
    {
        [Test]
        public void AcquirerAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickAcquirerMenuItem();
            AcquirerPage.CreateAcquirer();
            Assert.IsTrue(AcquirerPage.IsAcquirerwasCreated());
        }
    }
}
