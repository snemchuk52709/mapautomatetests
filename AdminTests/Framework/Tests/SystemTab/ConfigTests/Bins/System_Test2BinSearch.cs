﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.Bins
{
    class SystemTest2BinSearch : Base
    {
        [Test]
        public void BinSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickBinMenuItem();
            BinPage.SearchBin();
            Assert.IsTrue(BinPage.IsBinWasFound());
        }
    }

}
