﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.Bins
{
    class SystemTest3BinEdit : Base
    {
        [Test]
        public void BinEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickBinMenuItem();
            BinPage.EditBin();
            Assert.IsTrue(BinPage.IsBinWasEdited());
        }
    }
}
