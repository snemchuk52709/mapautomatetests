﻿using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab
{
    class RolePage
    {
        private const string CommonPermXPath = "//div[@id = 'addRoleDiv']//input[@class = 'check-box']";

        public static void ClickAddRoleBtn()
        {
            WebDriver.CommonActs.ClickBtn("divHeader");
        }

        //add new role part
        public static void AddRole_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtName", name);
        }

        public static void AddRole_IsSuperAdminPermission(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[0].Click();
        }

        public static void AddRole_IsEditSystemSettings(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[1].Click();
        }

        public static void AddRole_IsViewTransactions(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[2].Click();
        }

        public static void AddRole_IsCreateConfig(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[3].Click();
        }

        public static void AddRole_IsViewConfig(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[4].Click();
        }

        public static void AddRole_IsEditConfig(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[5].Click();
        }

        public static void AddRole_IsCreateCompany(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[6].Click();
        }

        public static void AddRole_IsViewCompany(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[7].Click();
        }

        public static void AddRole_IsEditCompany(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[8].Click();
        }

        public static void AddRole_IsCreateRoute(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[9].Click();
        }

        public static void AddRole_IsViewRoute(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[10].Click();
        }

        public static void AddRole_IsEditRoute(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[11].Click();
        }

        public static void AddRole_IsViewTerminal(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[12].Click();
        }

        public static void AddRole_IsCreateTerminal(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[13].Click();
        }

        public static void AddRole_IsEditTerminal(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[14].Click();
        }

        public static void AddRole_IsCreateUser(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[15].Click();
        }

        public static void AddRole_IsViewUser(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[16].Click();
        }

        public static void AddRole_IsEditUser(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[17].Click();
        }

        public static void AddRole_IsCreateFinanceTransaction(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[18].Click();
        }

        public static void AddRole_IsViewDailyTransactionSummary(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[19].Click();
        }

        public static void AddRole_IsViewMonthlyTransactionSummary(string isSet)
        {
            if (isSet == "yes") WebDriver.Driver.FindElements(By.XPath(CommonPermXPath))[20].Click();
        }

        public static void AddRole_ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnAdd");
        }

        //check role add
        public static bool AddRole_IsRoleWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search company part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtNameSearch", name);
        }

        public static void Search_ClickSeaarchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
        }
    }
}
