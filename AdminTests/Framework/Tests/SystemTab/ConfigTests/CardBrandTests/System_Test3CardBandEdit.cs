﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.CardBrandTests
{
    class SystemTest3CardBrandEdit : Base
    {
        [Test]
        public void CardBrandEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickCardBrandMenuItem();
            CardBrandPage.EditCardBrand();
            Assert.IsTrue(CardBrandPage.IsCardBrandWasEdited());
        }
    }
}
