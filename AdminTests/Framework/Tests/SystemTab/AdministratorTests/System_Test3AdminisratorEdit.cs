﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.AdministratorTests
{
    class SystemTest3AdministratorEdit : Base
    {
        [Test]
        public void AdministratorEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            Menu.SystemTab.ClickAdministrator();
            AdministratorPage.EditAdministrator();
            Assert.IsTrue(AdministratorPage.IsAdministratorWasEdited());
        }
    }
}
