﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
    class ModulePage
    {
        //add new Module part
        public static void ClickModuleAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddModule");
        }

        public static void AddModule_FillModuleFld(string moduleName)
        {
            WebDriver.CommonActs.FillTxtFld("txtModule", moduleName);
        }

        public static void AddModule_FillDescriptionFld(string description)
        {
            WebDriver.CommonActs.FillTxtFld("txtDescription", description);
        }

        public static void AddModule_FillFriendlyModuleNameFld(string friendlyModuleName)
        {
            WebDriver.CommonActs.FillTxtFld("txtFriendlyName", friendlyModuleName);
        }
        
        public static void AddModule_ClickAddNewHost()
        {
            WebDriver.CommonActs.ClickBtn("imgAddHost");
        }

        public static class AddNewHostWindow
        {
            public static void FillNameFld(string name)
            {
                WebDriver.CommonActs.FillTxtFld("txtHostName", name);
            }

            public static void IsEnabled(string isEnabled)
            {
                WebDriver.CommonActs.SetCheckboxStatus("chkHostEnabled", isEnabled);
            }

//update after code edit
            public static void ClickAddBtn()
            {
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'ui-dialog-buttonset']/button[1]")).Click();
            }
        }

        public static void AddModule_SelectHost(string text)
        {
            WebDriver.CommonActs.SelectValueInCmb("ddlHost", text);
        }

        public static void AddModule_SelectHostByIndex(int index)
        {
            WebDriver.CommonActs.SelectValueInCmbByIndex("ddlHost", index);
        }

        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //check Module creation
        public static bool AddModule_IsModulewasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search  part
        public static void Search_FillModuleFld(string module)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchModule", module);
        }

        public static void Search_FillDescriptionFld(string description)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchDesc", description);
        }

        public static void Search_IsShowDeletedModulesInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }
        
        public static string[] GetFirstModuleFromTable()
        {
            const string commonXPath = "//table[@id = 'tableModules']//tbody/tr[1]/td[";
            var module = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var description = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var friendlyModuleName = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var host = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var createdDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var createdBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var modifiedDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;
            var modifiedBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "8]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "9]")).Text;

            return new[] { module, description, friendlyModuleName, host, createdDate, createdBy, modifiedDate, modifiedBy, deleted };
        }

        //edit Module part
        public class EditModuleWindow
        {
            public static void SetNewValueToModule(string newModule)
            {
                WebDriver.CommonActs.FillTxtFld("txtModuleEdit", newModule);
            }

            public static void SetNewValueToDescrioption(string newDescrioption)
            {
                WebDriver.CommonActs.FillTxtFld("txtDescriptionEdit", newDescrioption);
            }

            public static void SelectNewValueForHost(string newHost)
            {
                WebDriver.CommonActs.SelectValueInCmb("ddlHostEdit", newHost);
            }

            public static void SelectNewValueForHostByIndex(int index)
            {
                WebDriver.CommonActs.SelectValueInCmbByIndex("ddlHostEdit", index);
            }

            public static void SetNewValueToFriendlyModuleName(string newFriendlyModuleName)
            {
                WebDriver.CommonActs.FillTxtFld("txtFriendlyNameEdit", newFriendlyModuleName);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete Module part
        public class DeleteModuleWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests
        //Module create
        protected static string CreateString;

        public static void CreateModule()
        {
            CreateString = WebDriver.CommonActs.RandomString();

            ClickModuleAddBtn();
            AddModule_FillModuleFld(CreateString);
            AddModule_FillDescriptionFld(CreateString);
            AddModule_FillFriendlyModuleNameFld(CreateString);

            AddModule_ClickAddNewHost();
            AddNewHostWindow.FillNameFld(CreateString);
            AddNewHostWindow.IsEnabled("yes");
            AddNewHostWindow.ClickAddBtn();
            AddModule_SelectHost(CreateString);
            ClickSubmitBtn();
        }

        public static bool IsModuleWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Module Search
        public static void SearchModule()
        {
            CreateModule();
            Search_FillModuleFld(CreateString);
            Search_FillDescriptionFld(CreateString);
            Search_IsShowDeletedModulesInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsModuleWasFound()
        {
            var firstRecordInThetable = GetFirstModuleFromTable();
            return CreateString == firstRecordInThetable[0] &
                   CreateString == firstRecordInThetable[2] &
                   CreateString == firstRecordInThetable[1];
        }

        //Module Edit
        protected static void ClickEditForFirstModuleFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableModules");
        }

        protected static string EditString;

        public static void EditModule()
        {
            CreateModule();

            EditString = WebDriver.CommonActs.RandomString();

            Search_FillModuleFld(CreateString);
            Search_FillDescriptionFld(CreateString);
            Search_ClickSearchBtn();
            ClickEditForFirstModuleFromTable();

            EditModuleWindow.SetNewValueToModule(EditString);
            EditModuleWindow.SetNewValueToDescrioption(EditString);

            if (CreateString != WebDriver.CommonActs.GetValueFromComboByIndex("ddlHostEdit", 1))
                EditModuleWindow.SelectNewValueForHostByIndex(1);
            EditModuleWindow.SelectNewValueForHostByIndex(2);
            EditModuleWindow.SetNewValueToFriendlyModuleName(EditString);
            EditModuleWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsModuleWasEdited()
        {
            Search_FillModuleFld(EditString);
            Search_FillDescriptionFld(EditString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstModuleFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Module Delete
        public static void ClickDeleteForFirstModuleFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableModules");
        }

        public static void DeleteModule()
        {
            CreateModule();
            Search_FillModuleFld(CreateString);
            Search_FillDescriptionFld(CreateString);
            Search_ClickSearchBtn();
            ClickDeleteForFirstModuleFromTable();
            DeleteModuleWindow.ClickYes();
        }

        public static bool IsModuleWasDeleted()
        {
            Search_FillModuleFld(CreateString);
            Search_FillDescriptionFld(CreateString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstModuleFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
