﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.HostTests
{
    class SystemTest2HostSearch : Base
    {
        [Test]
        public void HostSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickHostMenuItem();
            HostPage.SearchHost();
            Assert.IsTrue(HostPage.IsHostWasFound());
        }
    }

}
