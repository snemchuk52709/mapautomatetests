﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.HostTests
{
    class SystemTest3HostEdit : Base
    {
        [Test]
        public void HostEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickHostMenuItem();
            HostPage.EditHost();
            Assert.IsTrue(HostPage.IsHostWasEdited());
        }
    }
}
