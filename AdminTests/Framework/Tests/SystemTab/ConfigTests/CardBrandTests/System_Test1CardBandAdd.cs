﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.CardBrandTests
{
    class SystemTest1CardBrandAdd : Base
    {
        [Test]
        public void CardBrandAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickCardBrandMenuItem();
            CardBrandPage.CreateCardBrand();
            Assert.IsTrue(CardBrandPage.IsCardBrandWasCreated());
        }
    }
}
