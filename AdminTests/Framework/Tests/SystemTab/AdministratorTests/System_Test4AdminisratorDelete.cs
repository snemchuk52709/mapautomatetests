﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.AdministratorTests
{
    class SystemTest4AdministratorDelete : Base
    {
        [Test]
        public void AdministratorDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            Menu.SystemTab.ClickAdministrator();
            AdministratorPage.DeleteAdministrator();
            Assert.IsTrue(AdministratorPage.IsAdministratorWasDeleted());
        }
    }
}
