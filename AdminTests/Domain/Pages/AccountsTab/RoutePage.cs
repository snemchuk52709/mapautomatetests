﻿using AdminTests.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AdminTests.Domain.Pages.AccountsTab
{
    class RoutePage
    {
        public static void ClickAddRouteBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddRoute");
        }

        //add new route part
        public static void AddRoute_SelectCompany(string company)
        {
            WebDriver.CommonActs.SelectValueInCmb("CompanyIdAdd", company);
        }

        public static void AddRoute_SelectAcquirer(string acquirer)
        {
            WebDriver.CommonActs.SelectValueInCmb("AcquirerIdAdd", acquirer);
        }

        public static void AddRoute_SelectHost(string host)
        {
            WebDriver.CommonActs.SelectValueInCmb("HostIdAdd", host);
        }

        public static void AddRoute_SelectModule(string module)
        {
            WebDriver.CommonActs.SelectValueInCmb("ModuleIdAdd", module);
        }

        public static void AddRoute_FillHostMerchantIdFld(string hostMerchantId)
        {
            WebDriver.CommonActs.FillTxtFld("txtHostMerchantIdAdd", hostMerchantId);
        }

        public static void AddRoute_ChooseIsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("cbIsEnabledAdd", isEnabled);
        }

        public static void AddRoute_SelectBrand(string brand)
        {
            WebDriver.CommonActs.SelectValueInCmb("BrandIdAdd", brand);
        }

//update after code edit
        public static void AddRoute_SelectIs3dsEnbled(string is3DsEnbled)
        {
            var select = new SelectElement(WebDriver.Driver.FindElement(By.Id("Is3dsEnabledAdd")));
            if (is3DsEnbled == "yes")
                select.SelectByValue("true");
            select.SelectByValue("false");
        }

        public static void AddRoute_FillBinFld(string bin)
        {
            WebDriver.CommonActs.FillTxtFld("txtMidAdd", bin);
        }

        public static void AddRoute_FillAcqbinFld(string acqbin)
        {
            WebDriver.CommonActs.FillTxtFld("txtAcqbinAdd", acqbin);
        }

        public static void AddRoute_FillPasswordFld(string password)
        {
            WebDriver.CommonActs.FillTxtFld("txtPasswordAdd", password);
        }

        public static void AddRoute_ClickAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnBrandAdd");
        }

        //to be continued after code will be implemented...
    }
}
