﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.HostTests
{
    internal class SystemTest4HostDelete : Base
    {
        [Test]
        public void HostDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickHostMenuItem();
            HostPage.DeleteHost();
            Assert.IsTrue(HostPage.IsHostWasDeleted());
        }
    }
}
