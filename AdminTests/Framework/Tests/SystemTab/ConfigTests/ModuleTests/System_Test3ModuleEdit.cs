﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.ModuleTests
{
    class SystemTest3ModuleEdit : Base
    {
        [Test]
        public void ModuleEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickModuleMenuItem();
            ModulePage.EditModule();
            Assert.IsTrue(ModulePage.IsModuleWasEdited());
        }
    }
}
