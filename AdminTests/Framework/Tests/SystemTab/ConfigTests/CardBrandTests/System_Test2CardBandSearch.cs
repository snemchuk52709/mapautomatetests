﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.CardBrandTests
{
    class SystemTest2CardBrandSearch : Base
    {
        [Test]
        public void CardBrandSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickCardBrandMenuItem();
            CardBrandPage.SearchCardBrand();
            Assert.IsTrue(CardBrandPage.IsCardBrandWasFound());
        }
    }

}
