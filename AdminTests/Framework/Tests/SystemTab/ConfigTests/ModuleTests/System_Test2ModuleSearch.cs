﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.ModuleTests
{
    class SystemTest2ModuleSearch : Base
    {
        [Test]
        public void ModuleSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickModuleMenuItem();
            ModulePage.SearchModule();
            Assert.IsTrue(ModulePage.IsModuleWasFound());
        }
    }

}
