﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.Bins
{
    class SystemTest1BinAdd : Base
    {
        [Test]
        public void BinAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickBinMenuItem();
            BinPage.CreateBin();
            Assert.IsTrue(BinPage.IsBinWasCreated());
        }
    }
}
