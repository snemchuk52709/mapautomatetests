﻿using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages
{
    public static class LoginPage
    {
        public static void FillLoginFld(string login)
        {
            WebDriver.CommonActs.FillTxtFld("username", login);
        }

        public static void FillPasswordFld(string password)
        {
            WebDriver.CommonActs.FillTxtFld("password", password);
        }

        public static void ClickSignInBtn()
        {
            WebDriver.Driver.FindElement(By.XPath("//button[@type = 'submit']")).Click();
        }

        public static void DoLogin(string login, string password)
        {
            FillLoginFld(login);
            FillPasswordFld(password);
            ClickSignInBtn();
        }
    }
}
