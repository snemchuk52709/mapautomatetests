﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.AcquirerTests
{
    class SystemTest4AcquirerDelete : Base
    {
        [Test]
        public void AcquirerDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickAcquirerMenuItem();
            AcquirerPage.DeleteAcquirer();
            Assert.IsTrue(AcquirerPage.IsAcquirerWasDeleted());
        }
    }
}
