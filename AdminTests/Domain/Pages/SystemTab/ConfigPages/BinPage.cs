﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
    class BinPage
    {
        //add new Bin part
        public static void ClickBinAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddBin");
        }

        public static void AddBin_FillBinFld(string binName)
        {
            WebDriver.CommonActs.FillTxtFld("txtBin", binName);
        }

        public static void AddBin_FillDescriptionFld(string description)
        {
            WebDriver.CommonActs.FillTxtFld("txtDescription", description);
        }

        public static void AddBin_FillLengthFld(string length)
        {
            WebDriver.CommonActs.FillTxtFld("txtCardNumberLength", length);
        }

        public static void AddBin_FillRangeFld(string range)
        {
            WebDriver.CommonActs.FillTxtFld("txtRange", range);
        }

        public static void AddBin_ClickAddNewBrand()
        {
            WebDriver.CommonActs.ClickBtn("imgAddBrand");
        }

        public static class AddNewBrandWindow
        {
            public static void FillNameFld(string name)
            {
                WebDriver.CommonActs.FillTxtFld("txtBrandName", name);
            }

            public static void ChooseIsEnabled(string isEnabled)
            {
                WebDriver.CommonActs.SetCheckboxStatus("cbEnableBrand", isEnabled);
            }

//update after code edit
            public static void ClickAddBtn()
            {
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'ui-dialog-buttonset']/button[1]")).Click();
            }
        }

        public static void AddBin_SelectBrand(string text)
        {
            WebDriver.CommonActs.SelectValueInCmb("ddBrand", text);
        }

        public static void AddBin_SelectBrandByIndex(int index)
        {
            WebDriver.CommonActs.SelectValueInCmbByIndex("ddBrand", index);
        }

        public static void AddBin_IsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("chkEnable", isEnabled);
        }
        
        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //check Bin creation
        public static bool AddBin_IsBinwasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search  part
        public static void Search_FillBinFld(string bin)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchBin", bin);
        }

        public static void Search_FillDescriptionFld(string description)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchDesc", description);
        }

        public static void Search_FillLengthFld(string length)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchCardNoLen", length);
        }

        public static void Search_FillRangeFld(string range)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchRange", range);
        }

        public static void Search_IsShowDeletedBinsInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }
        
        protected static string[] GetFirstBinFromTable()
        {
            const string commonXPath = "//table[@id = 'tableBins']//tbody/tr[1]/td[";
            var bin = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var description = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var length = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var range = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var enabled = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var brandName = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;

            return new[] { bin, description, length, range, enabled, brandName, deleted };
        }

        //edit Bin part
        public class EditBinWindow
        {
            public static void SetNewValueToDescrioption(string newDescrioption)
            {
                WebDriver.CommonActs.FillTxtFld("txtDescriptionEdit", newDescrioption);
            }

            public static void SetNewValueToLength(string newLength)
            {
                WebDriver.CommonActs.FillTxtFld("txtCardNumberLengthEdit", newLength);
            }

            public static void SetNewValueToRange(string newRange)
            {
                WebDriver.CommonActs.FillTxtFld("txtRangeEdit", newRange);
            }

            public static void SelectNewValueForBrand(string newBrand)
            {
                WebDriver.CommonActs.SelectValueInCmb("ddBrandEdit", newBrand);
            }

            public static void SelectNewValueForBrandByIndex(int index)
            {
                WebDriver.CommonActs.SelectValueInCmbByIndex("ddBrandEdit", index);
            }

            public static void IsEnabled(string isEnabled)
            {
                WebDriver.CommonActs.SetCheckboxStatus("chkEnableEdit", isEnabled);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete Bin part
        public class DeleteBinWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests
        //Bin create
        protected static string CreateString;
        protected static string CreateNumber;

        public static void CreateBin()
        {
            CreateString = WebDriver.CommonActs.RandomString();
            CreateNumber = WebDriver.CommonActs.RandomNumber().ToString();

            ClickBinAddBtn();
            AddBin_FillBinFld(CreateNumber);
            AddBin_FillDescriptionFld(CreateString);
            AddBin_FillLengthFld(CreateNumber);
            AddBin_FillRangeFld(CreateNumber);

            AddBin_ClickAddNewBrand();
            AddNewBrandWindow.FillNameFld(CreateString);
            AddNewBrandWindow.ChooseIsEnabled("yes");
            AddNewBrandWindow.ClickAddBtn();

            AddBin_SelectBrand(CreateString);
            AddBin_IsEnabled("yes");
            ClickSubmitBtn();
        }

        public static bool IsBinWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Bin Search
        public static void SearchBin()
        {
            CreateBin();
            Search_FillBinFld(CreateNumber);
            Search_FillDescriptionFld(CreateString);
            Search_FillLengthFld(CreateNumber);
            Search_FillRangeFld(CreateNumber);
            Search_IsShowDeletedBinsInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsBinWasFound()
        {
            var firstRecordInThetable = GetFirstBinFromTable();
            return CreateNumber == firstRecordInThetable[0] &
                   CreateString == firstRecordInThetable[1] &
                   CreateNumber == firstRecordInThetable[2] &
                   CreateNumber == firstRecordInThetable[3];
        }

        //Bin Edit
        protected static void ClickEditForFirstBinFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableBins");
        }

        protected static string EditString;
        protected static string EditNumber;

        public static void EditBin()
        {
            CreateBin();

            EditString = WebDriver.CommonActs.RandomString();
            EditNumber = WebDriver.CommonActs.RandomNumber().ToString();

            Search_FillBinFld(CreateNumber);
            Search_FillDescriptionFld(CreateString);
            Search_FillLengthFld(CreateNumber);
            Search_FillRangeFld(CreateNumber);
            Search_ClickSearchBtn();
            ClickEditForFirstBinFromTable();

            EditBinWindow.SetNewValueToDescrioption(EditString);
            EditBinWindow.SetNewValueToLength(EditNumber);
            EditBinWindow.SetNewValueToRange(EditNumber);
            if (CreateString != WebDriver.CommonActs.GetValueFromComboByIndex("ddBrandEdit", 1))
                EditBinWindow.SelectNewValueForBrandByIndex(1);
            EditBinWindow.SelectNewValueForBrandByIndex(2);
            EditBinWindow.IsEnabled("yes");
            EditBinWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsBinWasEdited()
        {
            Search_FillBinFld(CreateNumber);
            Search_FillDescriptionFld(EditString);
            Search_FillLengthFld(EditNumber);
            Search_FillRangeFld(EditNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstBinFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Bin Delete
        public static void ClickDeleteForFirstBinFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableBins");
        }

        public static void DeleteBin()
        {
            CreateBin();
            Search_FillBinFld(CreateNumber);
            Search_FillDescriptionFld(CreateString);
            Search_FillLengthFld(CreateNumber);
            Search_FillRangeFld(CreateNumber);
            Search_ClickSearchBtn();
            ClickDeleteForFirstBinFromTable();
            DeleteBinWindow.ClickYes();
        }

        public static bool IsBinWasDeleted()
        {
            Search_FillBinFld(CreateNumber);
            Search_FillDescriptionFld(CreateString);
            Search_FillLengthFld(CreateNumber);
            Search_FillRangeFld(CreateNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstBinFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
