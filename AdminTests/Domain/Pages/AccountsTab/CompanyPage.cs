﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.AccountsTab
{
    class CompanyPage
    {
        public static void ClickAddCompanyBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddCompany");
        }

        //add new company part
        public static void AddCompany_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtName", name);
        }

        public static void AddCompany_FillAddressFld(string adress)
        {
            WebDriver.CommonActs.FillTxtFld("txtAddress", adress);
        }

        public static void AddCompany_FillTelephoneNoFld(string telephone)
        {
            WebDriver.CommonActs.FillTxtFld("txtTelephone", telephone);
        }

        public static void AddCompany_FillFaxFld(string fax)
        {
            WebDriver.CommonActs.FillTxtFld("txtFax", fax);
        }

        public static void AddCompany_FillEmailFld(string email)
        {
            WebDriver.CommonActs.FillTxtFld("txtEmail", email);
        }

        public static void AddCompany_FillBizRegFld(string bizReg)
        {
            WebDriver.CommonActs.FillTxtFld("txtBizReg", bizReg);
        }

        public static void AddCompany_FillRepresentativeFld(string representative)
        {
            WebDriver.CommonActs.FillTxtFld("txtRep", representative);
        }

        public static void AddCompany_SelectType(string selectedType)
        {
            WebDriver.CommonActs.SelectValueInCmb("ddlType", selectedType);
        }

        public static void AddCompany_SelectParentCompany(string selectedParentCompany)
        {
            WebDriver.CommonActs.SelectValueInCmb("ddlParentCompany", selectedParentCompany);
        }

        public static void AddCompany_SelectParentCompanyByIndex(int parentCompanyIndex)
        {
            WebDriver.CommonActs.SelectValueInCmbByIndex("ddlParentCompany", parentCompanyIndex);
        }

        public static void AddCompany_IsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("cbIsEnabled", isEnabled);
        }

        public static void AddCompany_ClickAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //search company part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtNameSearch", name);
        }

        public static void Search_FillAdressFld(string adress)
        {
            WebDriver.CommonActs.FillTxtFld("txtAddressSearch", adress);
        }

        public static void Search_FillTelephoneNoFld(string telephoneNo)
        {
            WebDriver.CommonActs.FillTxtFld("txtTelephoneSearch", telephoneNo);
        }

        public static void Search_FillFaxFld(string fax)
        {
            WebDriver.CommonActs.FillTxtFld("txtFaxSearch", fax);
        }

        public static void Search_FillEmailFld(string email)
        {
            WebDriver.CommonActs.FillTxtFld("txtEmailSearch", email);
        }

        public static void Search_FillBizRegFld(string bizReg)
        {
            WebDriver.CommonActs.FillTxtFld("txtBizRegSearch", bizReg);
        }

        public static void Search_FillRepresentativeFld(string representative)
        {
            WebDriver.CommonActs.FillTxtFld("txtRepSearch", representative);
        }

        public static void Search_IsShowDeletedCompaniesInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }

        //edit company part
        public class EditCompanyWindow
        {
            public static void SetNewValueToName(string newName)
            {
                WebDriver.CommonActs.FillTxtFld("txtNameEdit", newName);
            }

            public static void SetNewValueToAdress(string newAdress)
            {
                WebDriver.CommonActs.FillTxtFld("txtAddressEdit", newAdress);
            }

            public static void SetNewValueToTelephoneNo(string newTelephoneNo)
            {
                WebDriver.CommonActs.FillTxtFld("txtTelephoneEdit", newTelephoneNo);
            }

            public static void SetNewValueToFax(string newFax)
            {
                WebDriver.CommonActs.FillTxtFld("txtFaxEdit", newFax);
            }

            public static void SetNewValueToEmail(string newEmail)
            {
                WebDriver.CommonActs.FillTxtFld("txtEmailEdit", newEmail);
            }

            public static void SetNewValueToBizReg(string newBizReg)
            {
                WebDriver.CommonActs.FillTxtFld("txtBizRegEdit", newBizReg);
            }

            public static void SetNewValueToRepresentative(string newRepresentative)
            {
                WebDriver.CommonActs.FillTxtFld("txtRepEdit", newRepresentative);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
            }
        }

       //delete company part
       public class DeleteCompanyWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests

        //Company Create
        protected static string CreateEmail;
        protected static string CreateString;
        protected static string CreateNumber;
        
        public static void CreateCompany()
        {
            ClickAddCompanyBtn();

            CreateString = WebDriver.CommonActs.RandomString();
            CreateNumber = WebDriver.CommonActs.RandomNumber().ToString();
            CreateEmail = WebDriver.CommonActs.RandomEmail();

            AddCompany_FillNameFld(CreateString);
            AddCompany_FillAddressFld(CreateString);
            AddCompany_FillTelephoneNoFld(CreateNumber);
            AddCompany_FillFaxFld(CreateNumber);
            AddCompany_FillEmailFld(CreateEmail);
            AddCompany_FillBizRegFld(CreateNumber);
            AddCompany_FillRepresentativeFld(CreateNumber);
            AddCompany_SelectType("Merchant");
            AddCompany_SelectParentCompanyByIndex(2);
            AddCompany_IsEnabled("yes");
            AddCompany_ClickAddBtn();
        }

        public static bool IsCompanyWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Company Search
        public static void SearchCompany()
        {
            CreateCompany();
            Search_FillNameFld(CreateString);
            Search_FillAdressFld(CreateString);
            Search_FillTelephoneNoFld(CreateNumber);
            Search_FillFaxFld(CreateNumber);
            Search_FillEmailFld(CreateEmail);
            Search_FillBizRegFld(CreateNumber);
            Search_FillRepresentativeFld(CreateNumber);
            Search_IsShowDeletedCompaniesInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsCompanyWasFound()
        {
            var firstRecordInThetable = GetFirstCompanyFromTable();
            return CreateString == firstRecordInThetable[0] &
                   CreateString == firstRecordInThetable[1] &
                   CreateNumber == firstRecordInThetable[2] &
                   CreateNumber == firstRecordInThetable[3] &
                   CreateEmail == firstRecordInThetable[4] &
                   CreateNumber == firstRecordInThetable[5] &
                   CreateNumber == firstRecordInThetable[6];
        }

        //Company Edit
        public static void ClickEditForFirstCompanyFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableModules");
        }

        protected static string EditEmail;
        protected static string EditString;
        protected static string EditNumber;

        public static void EditCompany()
        {
            CreateCompany();

            EditEmail = WebDriver.CommonActs.RandomEmail();
            EditString = WebDriver.CommonActs.RandomString();
            EditNumber = WebDriver.CommonActs.RandomNumber().ToString();

            ClickEditForFirstCompanyFromTable();

            EditCompanyWindow.SetNewValueToName(EditString);
            EditCompanyWindow.SetNewValueToAdress(EditString);
            EditCompanyWindow.SetNewValueToTelephoneNo(EditNumber);
            EditCompanyWindow.SetNewValueToFax(EditNumber);
            EditCompanyWindow.SetNewValueToEmail(EditEmail);
            EditCompanyWindow.SetNewValueToBizReg(EditNumber);
            EditCompanyWindow.SetNewValueToRepresentative(EditNumber);
            EditCompanyWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsCompanyWasEdit()
        {
            Search_FillNameFld(EditString);
            Search_FillAdressFld(EditString);
            Search_FillTelephoneNoFld(EditNumber);
            Search_FillFaxFld(EditNumber);
            Search_FillEmailFld(EditEmail);
            Search_FillBizRegFld(EditNumber);
            Search_FillRepresentativeFld(EditNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstCompanyFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Company Delete
        public static void ClickDeleteForFirstCompanyFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableModules");
        }

        public static string[] GetFirstCompanyFromTable()
        {
            const string commonXPath = "//table[@id = 'tableModules']//tbody/tr[1]/td[";
            var name = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var adress = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var telephoneNo = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var fax = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var email = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var bizReg = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var representative = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;
            var type = WebDriver.Driver.FindElement(By.XPath(commonXPath + "9]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "8]")).Text;

            return new[] { name, adress, telephoneNo, fax, email, bizReg, representative, deleted, type };
        }
        
        public static void DeleteCompany()
        {
            CreateCompany();
            ClickDeleteForFirstCompanyFromTable();
            DeleteCompanyWindow.ClickYes();
        }

        public static bool IsCompanyWasDeleted()
        {
            Search_FillNameFld(CreateString);
            Search_FillAdressFld(CreateString);
            Search_FillTelephoneNoFld(CreateNumber);
            Search_FillFaxFld(CreateNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstCompanyFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
