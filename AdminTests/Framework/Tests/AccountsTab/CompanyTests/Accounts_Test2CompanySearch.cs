﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.AccountsTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.AccountsTab.CompanyTests
{
    class AccountsTest2CompanySearch : Base
    {
        [Test]
        public void CompanySearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.AccountsTab.ClickCompany();
            CompanyPage.SearchCompany();
            Assert.IsTrue(CompanyPage.IsCompanyWasFound());
        }
    }
}
