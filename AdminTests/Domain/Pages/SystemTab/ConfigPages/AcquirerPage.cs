﻿using System;
using System.Threading;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
    class AcquirerPage
    {
        //add new acquirer part
        public static void ClickAcquirerAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddAcquire");
        }
//update after code edit
        public static void AddAcquirer_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("Name", name);
        }
//update after code edit
        public static void AddAcquirer_FillLogicalIdFld(string ligicalId)
        {
            WebDriver.CommonActs.FillTxtFld("LogicalId", ligicalId);
        }
//update after code edit
        public static void AddAcquirer_ChooseIsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("Enabled", isEnabled);
        }

        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //search  part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchName", name);
        }

        public static void Search_FillLogicalIdFld(string ligicalId)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchLogicalId", ligicalId);
        }

        public static void Search_IsShowDeletedAcquiersInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }

        protected static string[] GetFirstAcquirerFromTable()
        {
            const string commonXPath = "//table[@id = 'tableAcquires']//tbody/tr[1]/td[";

            var name = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var logicalId = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var enabled = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var createdDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var createdBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var modifiedDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var modifiedBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "8]")).Text;

            return new[] { name, logicalId, enabled, createdDate, createdBy, modifiedDate, modifiedBy, deleted };
        }

        //check acquirer search
        public static bool Search_IsAcquirerWasFound(string name, string logicalId)
        {
            var firstRecordInThetable = GetFirstAcquirerFromTable();
            var isFound = name == firstRecordInThetable[0] &
                          logicalId == firstRecordInThetable[1];
            return isFound;
        }

        //edit acquirer part
        public class EditAcquirerWindow
        {
//update after code edit
            public static void SetNewValueToName(string newName)
            {
                //WebDriver.CommonActs.FillTxtFld("Name", newName);
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'modal-body']//input[@id = 'Name']")).Clear();
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'modal-body']//input[@id = 'Name']")).SendKeys(newName);
            }
//update after code edit
            public static void SetNewValueToLogicalId(string newLogicalId)
            {
                //WebDriver.CommonActs.FillTxtFld("LogicalId", newLogicalId);
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'modal-body']//input[@id = 'LogicalId']")).Clear();
                WebDriver.Driver.FindElement(By.XPath("//div[@class = 'modal-body']//input[@id = 'LogicalId']")).SendKeys(newLogicalId);
            }
//update after code edit
            public static void SetIsEnabled(string isEnabled)
            {
                //WebDriver.CommonActs.CheckCheckbox("Enabled");
                Thread.Sleep(100);
                //WebDriver.CommonActs.SetCheckboxStatus("cbIsEnabledEdit", isEnabled);
                if (isEnabled == "no") WebDriver.Driver.FindElement(By.XPath("//div[@class = 'modal-body']//input[@id = 'Enabled']")).Click();
            }
             

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete acquirer part
        public class DeleteAcquirerWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests
        //Acquirer create
        protected static string CreateString;
        protected static string CreateNumber;

        public static void CreateAcquirer()
        {
            CreateString = WebDriver.CommonActs.RandomString();
            CreateNumber = WebDriver.CommonActs.RandomNumber().ToString();

            ClickAcquirerAddBtn();
            AddAcquirer_FillNameFld(CreateString);
            AddAcquirer_FillLogicalIdFld(CreateNumber);
            AddAcquirer_ChooseIsEnabled("yes");
            ClickSubmitBtn();
        }

        public static bool IsAcquirerwasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Acquirer Search
        public static void SearchAcquirer()
        {
            CreateAcquirer();
            Search_FillNameFld(CreateString);
            Search_FillLogicalIdFld(CreateNumber);
            Search_IsShowDeletedAcquiersInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsAcquirerWasFound()
        {
            var firstRecordInThetable = GetFirstAcquirerFromTable();
            return CreateString == firstRecordInThetable[0] &
                   CreateNumber == firstRecordInThetable[1];
        }

        //Acquirer Edit
        public static void ClickEditForFirstAcquirerFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableAcquires");
        }

        protected static string EditString;
        protected static string EditNumber;

        public static void EditAcquirer()
        {
            CreateAcquirer();

            EditString = WebDriver.CommonActs.RandomString();
            EditNumber = WebDriver.CommonActs.RandomNumber().ToString();

            Search_FillNameFld(CreateString);
            Search_FillLogicalIdFld(CreateNumber);
            Search_ClickSearchBtn();
            ClickEditForFirstAcquirerFromTable();

            EditAcquirerWindow.SetNewValueToName(EditString);
            EditAcquirerWindow.SetNewValueToLogicalId(EditNumber);
            EditAcquirerWindow.SetIsEnabled("yes");
            EditAcquirerWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsAcquirerWasEdited()
        {
            Search_FillNameFld(EditString);
            Search_FillLogicalIdFld(EditNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstAcquirerFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Acquirer Delete
        public static void ClickDeleteForFirstAcquirerFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableAcquires");
        }

        public static void DeleteAcquirer()
        {
            CreateAcquirer();
            Search_FillNameFld(CreateString);
            Search_FillLogicalIdFld(CreateNumber);
            Search_ClickSearchBtn();
            ClickDeleteForFirstAcquirerFromTable();
            DeleteAcquirerWindow.ClickYes();
        }

        public static bool IsAcquirerWasDeleted()
        {
            Search_FillNameFld(CreateString);
            Search_FillLogicalIdFld(CreateNumber);
            Search_ClickSearchBtn();

            try
            {
                GetFirstAcquirerFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
