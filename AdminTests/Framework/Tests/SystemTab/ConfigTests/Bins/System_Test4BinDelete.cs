﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.Bins
{
    internal class SystemTest4BinDelete : Base
    {
        [Test]
        public void BinDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickBinMenuItem();
            BinPage.DeleteBin();
            Assert.IsTrue(BinPage.IsBinWasDeleted());
        }
    }
}
