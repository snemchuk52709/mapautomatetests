﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
    class HostPage
    {
        //add new Host part
        public static void ClickHostAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_hostAddHost");
        }

        public static void AddHost_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtName", name);
        }

        public static void AddHost_IsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("chkEnableHost", isEnabled);
        }

        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //check Host creation
        public static bool AddHost_IsHostwasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search  part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchName", name);
        }

        public static void Search_IsShowDeletedHostInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }

        public static string[] GetFirstHostFromTable()
        {
            const string commonXPath = "//table[@id = 'tableHosts']//tbody/tr[1]/td[";
            var name = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var createdDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var createdBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var modifiedDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var modifiedBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var enabled = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;

            return new[] { name, createdDate, createdBy, modifiedDate, modifiedBy, enabled, deleted };
        }

        //edit Host part
        public class EditHostWindow
        {
            public static void SetNewValueToName(string newName)
            {
                WebDriver.CommonActs.FillTxtFld("txtNameEdit", newName);
            }

            public static void IsEnabled(string isEnabled)
            {
                WebDriver.CommonActs.SetCheckboxStatus("chkEnableHostEdit", isEnabled);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete Host part
        public class DeleteHostWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests
        //Host create
        protected static string CreateString;

        public static void CreateHost()
        {
            CreateString = WebDriver.CommonActs.RandomString();

            ClickHostAddBtn();
            AddHost_FillNameFld(CreateString);
            AddHost_IsEnabled("yes");
            ClickSubmitBtn();
        }

        public static bool IsHostWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Host Search
        public static void SearchHost()
        {
            CreateHost();
            Search_FillNameFld(CreateString);
            Search_IsShowDeletedHostInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsHostWasFound()
        {
            var firstRecordInThetable = GetFirstHostFromTable();
            return CreateString == firstRecordInThetable[0];
        }

        //Host Edit
        protected static void ClickEditForFirstHostFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableHosts");
        }

        protected static string EditString;

        public static void EditHost()
        {
            CreateHost();

            EditString = WebDriver.CommonActs.RandomString();

            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();
            ClickEditForFirstHostFromTable();
            EditHostWindow.SetNewValueToName(EditString);
            EditHostWindow.IsEnabled("yes");
            EditHostWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsHostWasEdited()
        {
            Search_FillNameFld(EditString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstHostFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Host Delete
        public static void ClickDeleteForFirstHostFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableHosts");
        }

        public static void DeleteHost()
        {
            CreateHost();
            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();
            ClickDeleteForFirstHostFromTable();
            DeleteHostWindow.ClickYes();
        }

        public static bool IsHostWasDeleted()
        {
            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstHostFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
