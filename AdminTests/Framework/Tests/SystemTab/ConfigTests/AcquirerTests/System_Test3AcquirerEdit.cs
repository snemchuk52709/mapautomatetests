﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.AcquirerTests
{
    class SystemTest3AcquirerEdit : Base
    {
        [Test]
        public void AcquirerEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickAcquirerMenuItem();
            AcquirerPage.EditAcquirer();
            Assert.IsTrue(AcquirerPage.IsAcquirerWasEdited());
        }
    }
}
