﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.AdministratorTests
{
    class SystemTest2AdministratorSearch : Base
    {
        [Test]
        public void AdministratorSearchSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickAdministrator();
            AdministratorPage.SearchAdministrator();
            Assert.IsTrue(AdministratorPage.IsAdministratorWasFound());
        }
    }

}
