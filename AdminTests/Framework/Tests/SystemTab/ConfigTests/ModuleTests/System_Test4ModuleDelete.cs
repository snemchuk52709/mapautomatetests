﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.ModuleTests
{
    class SystemTest4ModuleDelete : Base
    {
        [Test]
        public void ModuleDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickModuleMenuItem();
            ModulePage.DeleteModule();
            Assert.IsTrue(ModulePage.IsModuleWasDeleted());
        }
    }
}
