﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.AccountsTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.AccountsTab.CompanyTests
{
    class AccountsTest3CompanyEdit : Base
    {
        [Test]
        public void CompanyEditSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.AccountsTab.ClickCompany();
            CompanyPage.EditCompany();
            Assert.IsTrue(CompanyPage.IsCompanyWasEdit());
        }
    }
}