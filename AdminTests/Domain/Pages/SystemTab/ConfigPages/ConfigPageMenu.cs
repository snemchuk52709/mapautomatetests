﻿using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
        public static class ConfigPageMenu
        {
            private const string CommonXPath = "//ul[@class = 'nav nav-list']/li[";

            public static void ClickAcquirerMenuItem()
            {
                WebDriver.Driver.FindElement(By.XPath(CommonXPath + "1]/a")).Click();
                //WebDriver.Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));
            }

            public static void ClickCardBrandMenuItem()
            {
                WebDriver.Driver.FindElement(By.XPath(CommonXPath + "2]/a")).Click();
            }

            public static void ClickBinMenuItem()
            {
                WebDriver.Driver.FindElement(By.XPath(CommonXPath + "3]/a")).Click();
            }

            public static void ClickHostMenuItem()
            {
                WebDriver.Driver.FindElement(By.XPath(CommonXPath + "4]/a")).Click();
            }

            public static void ClickModuleMenuItem()
            {
                WebDriver.Driver.FindElement(By.XPath("//ul[@class = 'nav nav-list']/li[5]/a")).Click();
            }
        }
}
