﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.AccountsTab;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.AccountsTab.CompanyTests
{
    class AccountsTest1CompanyAdd : Base
    {
        [Test]
        public void CompanyAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.AccountsTab.ClickCompany();
            CompanyPage.CreateCompany();
            Assert.IsTrue(CompanyPage.IsCompanyWasCreated());
        }
    }
}
