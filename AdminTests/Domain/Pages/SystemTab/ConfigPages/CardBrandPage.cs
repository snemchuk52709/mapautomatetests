﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab.ConfigPages
{
    class CardBrandPage
    {
        //add new CardBrand part
        public static void ClickCardBrandAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddBrand");
        }

        public static void AddCardBrand_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtName", name);
        }

        public static void AddCardBrand_IsEnabled(string isEnabled)
        {
            WebDriver.CommonActs.SetCheckboxStatus("chkEnableBrand", isEnabled);
        }

        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //check CardBrand creation
        public static bool AddCardBrand_IsCardBrandwasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search  part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtSearchName", name);
        }

        public static void Search_IsShowDeletedCardBrandInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeleted", isShowDeleted);
        }
        
        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }

        protected static string[] GetFirstCardBrandFromTable()
        {
            const string commonXPath = "//table[@id = 'tableBrands']//tbody/tr[1]/td[";
            var name = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var createdDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var createdBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;
            var modifiedDate = WebDriver.Driver.FindElement(By.XPath(commonXPath + "4]")).Text;
            var modifiedBy = WebDriver.Driver.FindElement(By.XPath(commonXPath + "5]")).Text;
            var enabled = WebDriver.Driver.FindElement(By.XPath(commonXPath + "6]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "7]")).Text;

            return new[] { name, createdDate, createdBy, modifiedDate, modifiedBy, enabled, deleted };
        }

        //edit CardBrand part
        public class EditCardBrandWindow
        {
            public static void SetNewValueToName(string newName)
            {
                WebDriver.CommonActs.FillTxtFld("txtNameEdit", newName);
            }

            public static void IsEnabled(string isEnabled)
            {
                WebDriver.CommonActs.SetCheckboxStatus("chkEnableBrandEdit", isEnabled);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete CardBrand part
        public class DeleteCardBrandWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }

        //methods for smoke tests
        //CardBrand create
        protected static string CreateString;

        public static void CreateCardBrand()
        {
            CreateString = WebDriver.CommonActs.RandomString();

            ClickCardBrandAddBtn();
            AddCardBrand_FillNameFld(CreateString);
            AddCardBrand_IsEnabled("yes");
            ClickSubmitBtn();
        }

        public static bool IsCardBrandWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //CardBrand Search
        public static void SearchCardBrand()
        {
            CreateCardBrand();
            Search_FillNameFld(CreateString);
            Search_IsShowDeletedCardBrandInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsCardBrandWasFound()
        {
            var firstRecordInThetable = GetFirstCardBrandFromTable();
            return CreateString == firstRecordInThetable[0];
        }

        //CardBrand Edit
        protected static void ClickEditForFirstCardBrandFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableBrands");
        }

        protected static string EditString;

        public static void EditCardBrand()
        {
            CreateCardBrand();

            EditString = WebDriver.CommonActs.RandomString();

            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();
            ClickEditForFirstCardBrandFromTable();
            EditCardBrandWindow.SetNewValueToName(EditString);
            EditCardBrandWindow.IsEnabled("yes");
            EditCardBrandWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsCardBrandWasEdited()
        {
            Search_FillNameFld(EditString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstCardBrandFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //CardBrand Delete
        public static void ClickDeleteForFirstCardBrandFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableBrands");
        }

        public static void DeleteCardBrand()
        {
            CreateCardBrand();
            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();
            ClickDeleteForFirstCardBrandFromTable();
            DeleteCardBrandWindow.ClickYes();
        }

        public static bool IsCardBrandWasDeleted()
        {
            Search_FillNameFld(CreateString);
            Search_ClickSearchBtn();

            try
            {
                GetFirstCardBrandFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
