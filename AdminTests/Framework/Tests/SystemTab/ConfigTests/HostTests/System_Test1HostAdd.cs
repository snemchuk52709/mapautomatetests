﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.HostTests
{
    class SystemTest1HostAdd : Base
    {
        [Test]
        public void HostAddSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickHostMenuItem();
            HostPage.CreateHost();
            Assert.IsTrue(HostPage.AddHost_IsHostwasCreated());
        }
    }
}
