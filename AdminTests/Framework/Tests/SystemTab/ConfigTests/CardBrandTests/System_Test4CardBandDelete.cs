﻿using AdminTests.Domain.CommonMenu;
using AdminTests.Domain.Pages;
using AdminTests.Domain.Pages.SystemTab.ConfigPages;
using NUnit.Framework;

namespace AdminTests.Framework.Tests.SystemTab.ConfigTests.CardBrandTests
{
    class SystemTest4CardBrandDelete : Base
    {
        [Test]
        public void CardBrandDeleteSuccessTest()
        {
            LoginPage.DoLogin(SmokeTestLogin, SmokeTestPassword);
            Menu.SystemTab.ClickConfig();
            ConfigPageMenu.ClickCardBrandMenuItem();
            CardBrandPage.DeleteCardBrand();
            Assert.IsTrue(CardBrandPage.IsCardBrandWasDeleted());
        }
    }
}
