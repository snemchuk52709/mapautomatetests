﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace AdminTests.Framework
{
    class WebDriver
    {
        public static IWebDriver Driver;

        public static void Close()
        {
            Driver.Quit();
        }

        public static void GoToUrl(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        public static string GetPageTitle()
        {
            return Driver.Title;
        }

        public static class CommonActs
        {
            private const int Timeout = 10000;

            public static void ClickBtn(string btnId)
            {
                Wait.UntilVisible(Driver.FindElement(By.Id(btnId)), Timeout).Click();
            }

            public static void FillTxtFld(string fldId, string value)
            {
                Wait.UntilVisible(Driver.FindElement(By.Id(fldId)), Timeout).Clear();
                Wait.UntilVisible(Driver.FindElement(By.Id(fldId)), Timeout).SendKeys(value);
            }

            public static void SelectValueInCmb(string selectCmbId, string text)
            {
                var select = new SelectElement(Wait.UntilVisible(Driver.FindElement(By.Id(selectCmbId)), Timeout));
                select.SelectByText(text);
            }

            public static void SelectValueInCmbByIndex(string selectCmbId, int index)
            {
                var select = new SelectElement(Wait.UntilVisible(Driver.FindElement(By.Id(selectCmbId)), Timeout));
                select.SelectByIndex(index);
            }

            public static void CheckCheckbox(string checkboxId)
            {
                Wait.UntilVisible(Driver.FindElement(By.Id(checkboxId)), Timeout).Click();
            }

            public static void SetCheckboxStatus(string checkboxId, string status)
            {
                if (status == "yes" & !Wait.UntilVisible(Driver.FindElement(By.Id(checkboxId)), Timeout).Selected)
                    Wait.UntilVisible(Driver.FindElement(By.Id(checkboxId)), Timeout).Click();
                if (status == "no" & Wait.UntilVisible(Driver.FindElement(By.Id(checkboxId)), Timeout).Selected)
                    Wait.UntilVisible(Driver.FindElement(By.Id(checkboxId)), Timeout).Click();
            }

            public static void WaitSpinnerLoading(string spinnerId)
            {
                var isLoading = true;
                var num = 0;
                do
                {
                    try { Driver.FindElement(By.Id(spinnerId)); }
                    catch (Exception)
                    {
                        Thread.Sleep(200);
                        isLoading = false;
                        num++;
                    }
                } while (isLoading && num < 10);
            }

            public static bool CheckRecordCreation()
            {
                return Driver.FindElement(By.Id("success-alert-content")).Text.Length > 0;
            }

            public static List<string> GetFirstRecordFromTable(string tableId)
            {
                var columnsNumber = Driver.FindElements(By.XPath("//table[@id = '" + tableId + "']//tbody/tr[1]/td")).Count;
                var firstRow = new List<string>();

                for (var i = 1; i <= columnsNumber; i++)
                {
                    firstRow.Add(Driver.FindElement(By.XPath("//table[@id = '" + tableId + "']//tbody/tr[1]/td[" + i + "]")).Text);
                }
                firstRow.Remove(firstRow.Last());
                return firstRow;
            }

            public static void ClickEditBtnForFirstRecordFromTable(string tableId)
            {
                Wait.UntilVisible(Driver.FindElement(By.XPath("//table[@id = '" + tableId + "']//tbody/tr[1]//i[@class = 'icon-pencil']")), Timeout).Click();
            }

            public static class EditWindowCommonBtns
            {
                public static void ClickUpdateBtn()
                {
                    ClickBtn("btnSubmitEdit");
                }
            }

            public static void ClickDeleteBtnForFirstRecordFromTable(string tableId)
            {
                Wait.UntilVisible(Driver.FindElement(By.XPath("//table[@id = '" + tableId + "']//tbody/tr[1]//i[@title= 'Delete']")), Timeout).Click();
            }

            public static class DeleteWindowCommonBtns
            {
                public static void ClickYes()
                {
                    ClickBtn("btnDelete");
                    WaitSpinnerLoading("loading-indicator-mask-overlay");
                }
            }

            public static void CommitEditReason()
            {
                Wait.UntilVisible(Driver.FindElement(By.Id("changeReason")), Timeout).Clear();
                Wait.UntilVisible(Driver.FindElement(By.Id("changeReason")), Timeout).SendKeys("test");
                ClickBtn("btnSubmitReason");
                WaitSpinnerLoading("loading-indicator-mask-overlay");
            }

            public static string RandomString()
            {
                var builder = new StringBuilder();
                var random = new Random();
                for (var i = 0; i < 8; i++)
                {
                    var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    builder.Append(ch);
                }
                return builder.ToString().ToLower();
            }

            public static int RandomNumber()
            {
                var random = new Random();
                return random.Next(100, 1000000);
            }

            public static string RandomEmail()
            {
                var builder = new StringBuilder();
                var random = new Random();
                for (var i = 0; i < 6; i++)
                {
                    var ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    builder.Append(ch);
                }
                builder.Append("@ss.com");
                return builder.ToString().ToLower();
            }

            public static string GetSelectedValueFromCombo(string cmbId)
            {
                var select = new SelectElement(Driver.FindElement(By.Id(cmbId)));
                return select.SelectedOption.Text;
            }

            public static string GetValueFromComboByIndex(string cmbId, int index)
            {
                var select = new SelectElement(Driver.FindElement(By.Id(cmbId)));
                select.SelectByIndex(index);
                return GetSelectedValueFromCombo(cmbId);
            }

            public static class Wait
            {
                public static IWebElement UntilVisible(IWebElement element, TimeSpan timeOut)
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    while (true)
                    {
                        WebDriverException lastException = null;
                        try
                        {
                            if (element.Displayed)
                            {
                                return element;
                            }
                            Thread.Sleep(100);
                        }
                        catch (ElementNotVisibleException e) { lastException = e; }
                        catch (NoSuchElementException e) { lastException = e; }
                        catch (StaleElementReferenceException e) { lastException = e; }

                        if (sw.Elapsed > timeOut)
                        {
                            var exceptionMessage = lastException == null ? "" : lastException.Message;
                            var errorMessage = string.Format("Wait.UntilVisible: Element was not displayed after {0} Milliseconds" +
                                                        "\r\n Error Message:\r\n{1}", timeOut.TotalMilliseconds, exceptionMessage);
                            throw new TimeoutException(errorMessage);
                        }
                    }
                }

                public static IWebElement UntilVisible(IWebElement element, int timeOutMilliseconds)
                {
                    return UntilVisible(element, TimeSpan.FromMilliseconds(timeOutMilliseconds));
                }
            }
        }
    }
}
