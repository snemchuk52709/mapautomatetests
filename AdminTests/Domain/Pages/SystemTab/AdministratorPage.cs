﻿using System;
using AdminTests.Framework;
using OpenQA.Selenium;

namespace AdminTests.Domain.Pages.SystemTab
{
    class AdministratorPage
    {
        //add new Administrator part
        public static void ClickAdministratorAddBtn()
        {
            WebDriver.CommonActs.ClickBtn("divExpTitle_divAddAdministrator");
        }

        public static void AddAdministrator_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtAdminName", name);
        }

        public static void AddAdministrator_FillEmailFld(string email)
        {
            WebDriver.CommonActs.FillTxtFld("txtEmail", email);
        }

        public static void AddAdministrator_FillPasswordFld(string password)
        {
            WebDriver.CommonActs.FillTxtFld("txtPassword", password);
        }

        public static void AddAdministrator_FillConfirmPasswordFld(string confirmPassword)
        {
            WebDriver.CommonActs.FillTxtFld("txtConfirmPassword", confirmPassword);
        }

        public static void AddAdministrator_SelectRole(string role)
        {
            WebDriver.CommonActs.SelectValueInCmb("RoleId", role);
        }

        public static void AddAdministrator_SelectRoleByIndex(int index)
        {
            WebDriver.CommonActs.SelectValueInCmbByIndex("RoleId", index);
        }

        public static void ClickSubmitBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSubmit");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
        }

        //check Administrator creation
        public static bool AddAdministrator_IsAdministratorWasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //search  part
        public static void Search_FillNameFld(string name)
        {
            WebDriver.CommonActs.FillTxtFld("txtName", name);
        }

        public static void Search_FillRoleFld(string role)
        {
            WebDriver.CommonActs.FillTxtFld("txtRole", role);
        }

        public static void Search_IsShowDeletedAdministratorInResult(string isShowDeleted)
        {
            WebDriver.CommonActs.SetCheckboxStatus("dbShowDeletedAdmin", isShowDeleted);
        }

        public static void Search_ClickSearchBtn()
        {
            WebDriver.CommonActs.ClickBtn("btnSearch");
            WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-page_main_content-overlay");
        }

        protected static string[] GetFirstAdministratorFromTable()
        {
            const string commonXPath = "//table[@id = 'tableUsers']//tbody/tr[1]/td[";
            var name = WebDriver.Driver.FindElement(By.XPath(commonXPath + "1]")).Text;
            var role = WebDriver.Driver.FindElement(By.XPath(commonXPath + "2]")).Text;
            var deleted = WebDriver.Driver.FindElement(By.XPath(commonXPath + "3]")).Text;

            return new[] { name, role, deleted };
        }

        //edit Administrator part
        public class EditAdministratorWindow
        {
            public static void SelectNewRole(string role)
            {
                WebDriver.CommonActs.SelectValueInCmb("EditRoleId", role);
            }

            public static void SelectNewRoleByIndex(int index)
            {
                WebDriver.CommonActs.SelectValueInCmbByIndex("EditRoleId", index);
            }

            public static void ClickUpdateBtn()
            {
                WebDriver.CommonActs.ClickBtn("btnSubmitEdit");
                WebDriver.CommonActs.WaitSpinnerLoading("loading-indicator-mask-overlay");
            }
        }

        //delete Administrator part
        public class DeleteAdministratorWindow
        {
            public static void ClickYes()
            {
                WebDriver.CommonActs.DeleteWindowCommonBtns.ClickYes();
            }
        }
       
        //methods for smoke tests
        //Administrator create
        protected static string CreateString;
        protected static string CreateEmail;
        protected static string CreateRole;

        public static void CreateAdministrator()
        {
            CreateString = WebDriver.CommonActs.RandomString();
            CreateEmail = WebDriver.CommonActs.RandomEmail();

            ClickAdministratorAddBtn();
            AddAdministrator_FillNameFld(CreateString);
            AddAdministrator_FillEmailFld(CreateEmail);
            AddAdministrator_FillPasswordFld(CreateString);
            AddAdministrator_FillConfirmPasswordFld(CreateString);
            AddAdministrator_SelectRoleByIndex(1);

            CreateRole = WebDriver.CommonActs.GetSelectedValueFromCombo("RoleId");
            ClickSubmitBtn();
        }

        public static bool IsAdministratorwasCreated()
        {
            return WebDriver.CommonActs.CheckRecordCreation();
        }

        //Administrator Search
        public static void SearchAdministrator()
        {
            CreateAdministrator();
            Search_FillNameFld(CreateString);
            Search_FillRoleFld(CreateRole);
            Search_IsShowDeletedAdministratorInResult("yes");
            Search_ClickSearchBtn();
        }

        public static bool IsAdministratorWasFound()
        {
            var firstRecordInThetable = GetFirstAdministratorFromTable();
            return CreateString == firstRecordInThetable[0] &
                   CreateRole == firstRecordInThetable[1];
        }

        //Administrator Edit
        public static void ClickEditForFirstAdministratorFromTable()
        {
            WebDriver.CommonActs.ClickEditBtnForFirstRecordFromTable("tableUsers");
        }
        
        protected static string EditSelectedRole;

        public static void EditAdministrator()
        {
            CreateAdministrator();
            Search_FillNameFld(CreateString);
            Search_FillRoleFld(CreateRole);
            Search_ClickSearchBtn();
            ClickEditForFirstAdministratorFromTable();

            if (CreateRole != WebDriver.CommonActs.GetValueFromComboByIndex("EditRoleId", 1))
            EditAdministratorWindow.SelectNewRoleByIndex(1);
            EditAdministratorWindow.SelectNewRoleByIndex(2);

            EditSelectedRole = WebDriver.CommonActs.GetSelectedValueFromCombo("EditRoleId");
            EditAdministratorWindow.ClickUpdateBtn();
            WebDriver.CommonActs.CommitEditReason();
        }

        public static bool IsAdministratorWasEdited()
        {
            Search_FillNameFld(CreateString);
            Search_FillRoleFld(EditSelectedRole);
            Search_ClickSearchBtn();

            try
            {
                GetFirstAdministratorFromTable();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Administrator Delete
        public static void ClickDeleteForFirstAdministratorFromTable()
        {
            WebDriver.CommonActs.ClickDeleteBtnForFirstRecordFromTable("tableUsers");
        }

        public static void DeleteAdministrator()
        {
            CreateAdministrator();
            Search_FillNameFld(CreateString);
            Search_FillRoleFld(CreateRole);
            Search_ClickSearchBtn();
            ClickDeleteForFirstAdministratorFromTable();
            DeleteAdministratorWindow.ClickYes();
        }

        public static bool IsAdministratorWasDeleted()
        {
            Search_FillNameFld(CreateString);
            Search_FillRoleFld(CreateRole);
            Search_ClickSearchBtn();

            try
            {
                GetFirstAdministratorFromTable();
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
    }
}
